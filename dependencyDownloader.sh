#!/bin/bash

declare -A dependencies=(
  ["https://cdn.tailwindcss.com"]="./tailwind.min.js"
  ["https://unpkg.com/alpinejs@latest/dist/cdn.min.js"]="./alpine.min.js"
  ["https://unpkg.com/@imacrayon/alpine-ajax@latest/dist/cdn.min.js"]="./alpine-ajax.min.js"
  ["https://unpkg.com/@imacrayon/alpine-ajax@latest/dist/server.js"]="./alpine-ajax-mock-server.js"
)

for key in ${!dependencies[@]}; do
  wget $key
  generated=$(ls -c | head -n 1)
  mv $generated ${dependencies[$key]}
done
