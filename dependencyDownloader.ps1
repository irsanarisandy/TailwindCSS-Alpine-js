$myHash = @{
  'https://cdn.tailwindcss.com' = './tailwind.min.js';
  'https://unpkg.com/alpinejs@latest/dist/cdn.min.js' = './alpine.min.js';
  'https://unpkg.com/@imacrayon/alpine-ajax@latest/dist/cdn.min.js' = './alpine-ajax.min.js';
  'https://unpkg.com/@imacrayon/alpine-ajax@latest/dist/server.js' = './alpine-ajax-mock-server.js';
}

ForEach ($file in $myHash.GetEnumerator()) {
  Invoke-WebRequest -Uri $file.Key -OutFile $file.Value
}
