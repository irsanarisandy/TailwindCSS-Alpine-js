document.addEventListener("alpine:init", () => {
  Alpine.store("darkMode", false);
});

// Using mock server, as local files can't make real AJAX request due to CORS
// Returned element must include same id as target
route("GET", "/ajax-demo", () => {
  return `<output id="ajax-result">Now, get back to work!!!</output>`;
});
